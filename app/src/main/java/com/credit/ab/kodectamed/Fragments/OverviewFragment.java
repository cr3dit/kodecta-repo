package com.credit.ab.kodectamed.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.credit.ab.kodectamed.Adapters.QuestionnaireAdapter;
import com.credit.ab.kodectamed.Adapters.TherapyAdapter;
import com.credit.ab.kodectamed.Model.Questionnaire;
import com.credit.ab.kodectamed.Model.Therapy;
import com.credit.ab.kodectamed.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.jmedeisis.draglinearlayout.DragLinearLayout;

import java.util.ArrayList;
import java.util.List;

public class OverviewFragment  extends Fragment  {
    TextView txtWelcome;
    FirebaseAuth mAuth;
    
    ImageView imageViewCall;
    TherapyAdapter therapyAdapter;
    QuestionnaireAdapter questionnaireAdapter;
    TextView dismissText;
    RecyclerView recyclerView;
    RecyclerView recyclerViewQuestionnaire;
    RelativeLayout welcomePlaceholder;

    private List<Therapy> therapyList = new ArrayList<>();
    private List<Questionnaire> questionnaireList =new ArrayList<>();

    public OverviewFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View overviewFrag = inflater.inflate(R.layout.fragment_overview, container, false);
        dismissText=overviewFrag.findViewById(R.id.dismissLabel);
        recyclerView= overviewFrag.findViewById(R.id.recycler_view);
        recyclerViewQuestionnaire =overviewFrag.findViewById(R.id.recycler_view_questionnary);
        welcomePlaceholder=overviewFrag.findViewById(R.id.welcomePlaceholder);
        therapyAdapter=new TherapyAdapter(therapyList);
        questionnaireAdapter =new QuestionnaireAdapter(questionnaireList);



        recyclerViewQuestionnaire.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewQuestionnaire.setAdapter(questionnaireAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(therapyAdapter);

        prepareData();

        return overviewFrag;
    }

    @SuppressLint("SetTextI18n")
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        txtWelcome=getActivity().findViewById(R.id.textWelcome);
        imageViewCall=getActivity().findViewById(R.id.imageViewCall);

        imageViewCall.setOnClickListener(v -> {
            String phone_number="061610402";
            Intent i = new Intent(Intent.ACTION_DIAL);
            String p = "tel:" + phone_number;
            i.setData(Uri.parse(p));
            startActivity(i);
        });
        if(currentUser!=null) {

            txtWelcome.setText("Hello " + currentUser.getDisplayName() + ",");
        }
        else txtWelcome.setText(R.string.welcome);

        DragLinearLayout dragLinearLayout =  getActivity().findViewById(R.id.container_layout);

         for(int i = 2; i < dragLinearLayout.getChildCount(); i++){
                View child = dragLinearLayout.getChildAt(i);
              // the child will act as its own drag handle
                 dragLinearLayout.setViewDraggable(child, child);
              }
        dismissText.setOnClickListener(v -> welcomePlaceholder.setVisibility(View.GONE));


}
    private void prepareData() {
        Therapy therapy = new Therapy("09:30", "Physiotherapy", "Raum 505, Block F");
        therapyList.add(therapy);

        therapy = new Therapy("10:30", "Jogging", "Raum 3200, Building A");
        therapyList.add(therapy);

        therapy = new Therapy("15:00", "Physiotherapy", "Raum 2141, Pavilion 25");
        therapyList.add(therapy);

        Questionnaire questionnaire =new Questionnaire("Anamnesis",16,9);
        questionnaireList.add(questionnaire);

        questionnaire =new Questionnaire("Feedback",12,3);
        questionnaireList.add(questionnaire);

        questionnaire =new Questionnaire("Scoring",16,3);
        questionnaireList.add(questionnaire);

       therapyAdapter.notifyDataSetChanged();
       questionnaireAdapter.notifyDataSetChanged();
    }


}