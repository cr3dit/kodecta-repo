package com.credit.ab.kodectamed;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GithubAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity
        {

    private static final String TAG = "Login";
    private SecureRandom random = new SecureRandom();
    private FirebaseAuth mAuth;
    private TwitterLoginButton mLoginTwitterButton;
    Button mLoginGitHubButton;
    String REDIRECT_URL_CALLBACK = "https://kodectamed.firebaseapp.com/__/auth/handler";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Configure Twitter SDK
        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                getString(R.string.twitter_consumer_key),
                getString(R.string.twitter_consumer_secret));
        TwitterConfig twitterConfig = new TwitterConfig.Builder(this)
                .twitterAuthConfig(authConfig)
                .build();
        Twitter.initialize(twitterConfig);

        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        mLoginTwitterButton = findViewById(R.id.button_twitter_login);
        mLoginGitHubButton = findViewById(R.id.button_github_login);


        mLoginGitHubButton.setOnClickListener(v -> handleGitHubSession());


        mLoginTwitterButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.d(TAG, "twitterLogin:success" + result);
                handleTwitterSession(result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Log.w(TAG, "twitterLogin:failure", exception);
            }
        });
        // [END initialize_twitter_login]


        //start github redirect

        Uri uri = getIntent().getData();
        if (uri != null && uri.toString().startsWith("https://kodectamed.firebaseapp.com/__/auth/handler")) {
            String code = uri.getQueryParameter("code");
            String state = uri.getQueryParameter("state");
            if (code != null && state != null)
                sendPost(code, state);

        }

    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }
    // [END on_start_check_user]

    // [START on_activity_result]
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the Twitter login button.
        mLoginTwitterButton.onActivityResult(requestCode, resultCode, data);
    }
    // [END on_activity_result]

    // [START auth_with_twitter]
    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);
        // [START_EXCLUDE silent]
        // [END_EXCLUDE]

        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        Toast.makeText(LoginActivity.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }

                    // [START_EXCLUDE]
                    // [END_EXCLUDE]
                });
    }
    // [END auth_with_twitter]


    ///AUTH WITH GITHUB


    private void handleGitHubSession() {

        HttpUrl httpUrl = new HttpUrl.Builder().
                scheme("http").
                host("github.com").
                addPathSegment("login").
                addPathSegment("oauth").
                addPathSegment("authorize").
                addQueryParameter("client_id", "c3f145772f5d7584821b").
                addQueryParameter("redirect_uri", REDIRECT_URL_CALLBACK).
                addQueryParameter("state", getRandomString()).
                addQueryParameter("scope", "read:user").
                build();

        Log.d(TAG, httpUrl.toString());

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(httpUrl.toString()));
        startActivity(intent);

    }

    private String getRandomString() {
        return new BigInteger(130, random).toString(32);

    }



    private void sendPost(String code, String state) {
        //POST http github login auth acces token
        OkHttpClient okHttpClient = new OkHttpClient();
        FormBody form = new FormBody.Builder().
                add("client_id", "c3f145772f5d7584821b").
                add("client_secret", "0e5ea4dfe4e293474d9e75a3e4e0df070dee02f7").
                add("code", code).
                add("redirect_uri", REDIRECT_URL_CALLBACK).
                add("state", state).
                build();
        Request request = new Request.Builder().
                url("https://github.com/login/oauth/access_token").
                post(form).
                build();
        okHttpClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {

            }


            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                String responseBody = Objects.requireNonNull(response.body()).string();

                String[] splitted = responseBody.split("=|$");
                if (splitted[0].equalsIgnoreCase("access_token"))
                    signInWithToken(splitted[1]);
                else
                    Toast.makeText(LoginActivity.this, "splitted[0]=>" + splitted[0], Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void signInWithToken(String token) {

        AuthCredential credential = GithubAuthProvider.getCredential(token);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));

                    // If sign in fails, display a message to the user. If sign in succeeds
                    // the auth state listener will be notified and logic to handle the
                    // signed in user can be handled in the listener.
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "signInWithCredential", task.getException());


                        // ...
                    }
                });
    }

}
