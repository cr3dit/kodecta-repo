package com.credit.ab.kodectamed.Model;

public class Therapy  {
    private String time, therapyName, location;


    public Therapy(String time, String therapyName, String location) {
        this.time = time;
        this.therapyName = therapyName;
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTherapyName() {
        return therapyName;
    }

    public void setTherapyName(String therapyName) {
        this.therapyName = therapyName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}