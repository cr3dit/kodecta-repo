package com.credit.ab.kodectamed.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.credit.ab.kodectamed.Model.Questionnaire;
import com.credit.ab.kodectamed.R;

import java.util.List;

public class QuestionnaireAdapter extends RecyclerView.Adapter<QuestionnaireAdapter.MyViewHolder> {

private List<Questionnaire> questionnaireList;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView answers, therapyName, percentage;
    ProgressBar progressBar;



     MyViewHolder(View view) {
        super(view);
        answers =  view.findViewById(R.id.answers);
        percentage =  view.findViewById(R.id.percentage);
        therapyName = view.findViewById(R.id.therapyName);
        progressBar=view.findViewById(R.id.progressBar);

     }
}


    public QuestionnaireAdapter(List<Questionnaire> questionnaireList) {
        this.questionnaireList = questionnaireList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.questionnary_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Questionnaire questionnaire = questionnaireList.get(position);
        int minNumber= questionnaire.getMinQuest();
        int maxNumber= questionnaire.getMaxQuest();

        holder.progressBar.setMax(maxNumber);
        holder.progressBar.setProgress(minNumber);
        holder.percentage.setText(calculatePercentage(minNumber,maxNumber)+" %");
        holder.answers.setText(questionnaire.getMinQuest()+"/"+ questionnaire.getMaxQuest());
        holder.therapyName.setText(questionnaire.getQuestionnaireName());
    }

    @Override
    public int getItemCount() {
        return questionnaireList.size();
    }

    private int calculatePercentage(int minNum, int maxNum){
    int value;

    value=100*minNum;
    value=value/maxNum;


    return value;
    }

}
