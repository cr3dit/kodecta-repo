package com.credit.ab.kodectamed;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;


import com.credit.ab.kodectamed.Fragments.OverviewFragment;
import com.credit.ab.kodectamed.Fragments.TherapiesFragment;

import com.twitter.sdk.android.core.TwitterCore;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private ActionBar toolbar;
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        toolbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        toolbar.setCustomView(R.layout.custom_actionbar);
         //       .setIcon(R.drawable.mdoc_white_logo);

        loadFragment(new OverviewFragment());


    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_overview:
                    toolbar.setTitle("Overview");
                    fragment = new OverviewFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_therapieplan:
                    toolbar.setTitle("Therapieplan");
                    fragment = new TherapiesFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_essen:
                    toolbar.setTitle("Essen");
                    return true;
                case R.id.navigation_notifications:
                    toolbar.setTitle("Notifications");
                    return true;
                case R.id.navigation_mehr:
                    toolbar.setTitle("Mehr");
                    return true;
            }
            return false;
        }
    };
    @SuppressLint("RestrictedApi")
    public static void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            //Timber.e(e, "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            //Timber.e(e, "Unable to change value of shift mode");
        }
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }



    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i==R.id.buttonLogout){
            signOut();
        }

    }
    public void onBackPressed(){
      finish();
    }

    private void signOut() {
        //mAuth.signOut();
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));

    }
}



