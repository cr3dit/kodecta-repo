package com.credit.ab.kodectamed.Model;

public class Questionnaire {
    private String questionnaireName;
    private int maxQuest, minQuest;


    public Questionnaire(String questionnaireName, int  maxQuest, int minQuest) {
        this.questionnaireName = questionnaireName;
        this.maxQuest = maxQuest;
        this.minQuest = minQuest;
    }

    public String getQuestionnaireName() {
        return questionnaireName;
    }



    public int getMaxQuest() {
        return maxQuest;
    }


    public int getMinQuest() {
        return minQuest;
    }

}