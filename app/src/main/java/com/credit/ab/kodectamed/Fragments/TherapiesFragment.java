package com.credit.ab.kodectamed.Fragments;

import android.content.Intent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.credit.ab.kodectamed.LoginActivity;
import com.credit.ab.kodectamed.R;
import com.google.firebase.auth.FirebaseAuth;
import com.twitter.sdk.android.core.TwitterCore;

public class TherapiesFragment extends Fragment {
    Button logoutButton;
    private FirebaseAuth mAuth;

    public TherapiesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_therapieplan, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        logoutButton=getActivity().findViewById(R.id.buttonLogout);



        logoutButton.setOnClickListener(v -> {
            mAuth = FirebaseAuth.getInstance();
            mAuth.signOut();
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
             startActivity(new Intent(getActivity(), LoginActivity.class));

        });
    }

}