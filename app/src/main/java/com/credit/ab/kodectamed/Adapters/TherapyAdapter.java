package com.credit.ab.kodectamed.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.credit.ab.kodectamed.Model.Therapy;
import com.credit.ab.kodectamed.R;

import java.util.List;

public class TherapyAdapter extends RecyclerView.Adapter<TherapyAdapter.MyViewHolder> {

private List<Therapy> therapyList;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView time, therapyName, location;

     MyViewHolder(View view) {
        super(view);
        time =  view.findViewById(R.id.answers);
        location =  view.findViewById(R.id.location);
        therapyName =  view.findViewById(R.id.therapyName);
    }
}


    public TherapyAdapter(List<Therapy> therapyList) {
        this.therapyList = therapyList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.therapy_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Therapy therapy = therapyList.get(position);
        holder.time.setText(therapy.getTime());
        holder.location.setText(therapy.getLocation());
        holder.therapyName.setText(therapy.getTherapyName());
    }

    @Override
    public int getItemCount() {
        return therapyList.size();
    }
}